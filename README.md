# [TOOLS & RULES](https://tools4rules.bitbucket.io)#
{ Repository for class & workshops }

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---


### What is this repository for? ###

* The Tools
* The Rules
* [website](https://tools4rules.bitbucket.io)

![image.png](https://bitbucket.org/repo/8X5nEBG/images/4274184795-image.png)

## Presentation

Tools and Rules is a concise pedagogical framework for students to experiment with computational graphic design. 
It has been specifically designed as a visual research teaching course enabling students to create within 
a strict and methodological manner. The framework includes a set of custom-made, open source tools along with a set of rules.

## Context

This project comes from a whole mix of thoughts on tool making. 

## Contact & Sundries

* mark.webster[at]wanadoo.fr
* Version v0.95
* Tools used : Processing

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/